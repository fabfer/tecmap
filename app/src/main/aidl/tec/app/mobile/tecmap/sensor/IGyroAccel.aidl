package tec.app.mobile.tecmap.sensor;

interface IGyroAccel {
  oneway void diff( in double x, in double y, in double z );
}
