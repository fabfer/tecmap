package gui;

import android.view.View;
import android.view.ViewGroup;

import commands.Command;
import system.Container;

/**
 * Every object which has to be displayed in a {@link gui.CustomListActivity} has to
 * implement this interface. Also see {@link system.Container}.
 *
 * @author Spobo
 */
public interface ListItem {

    /**
     * @see android.widget.Adapter#getView(int, android.view.View,
     * android.view.ViewGroup)
     */
    View getMyListItemView(View viewToUseIfNotNull, ViewGroup parentView);

    /**
     * @return normally this should return the default onClick command if the
     * class already has one
     */
    Command getListClickCommand();

    Command getListLongClickCommand();

}
