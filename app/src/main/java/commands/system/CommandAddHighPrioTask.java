package commands.system;

import commands.Command;
import system.TaskManager;

public class CommandAddHighPrioTask extends Command {

    private Command myCommandToAdd;

    public CommandAddHighPrioTask(Command commandToAdd) {
        myCommandToAdd = commandToAdd;
    }

    @Override
    public boolean execute() {
        TaskManager.getInstance().addHighPrioTask(myCommandToAdd);
        return true;
    }

}
