package tec.app.mobile.tecmap.setup;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import actions.Action;
import actions.ActionCalcRelativePos;
import actions.ActionRotateCameraBuffered;
import actions.ActionWaitForAccuracy;
import commands.Command;
import geo.GeoObj;
import gl.CustomGLSurfaceView;
import gl.GL1Renderer;
import gl.GLCamera;
import gl.GLFactory;
import gl.HasPosition;
import gl.animations.AnimationFaceToCamera;
import gl.scenegraph.MeshComponent;
import gl.textures.TexturedShape;
import gui.RadarView;
import gui.simpleUI.ModifierGroup;
import gui.simpleUI.Theme;
import gui.simpleUI.modifiers.Headline;
import gui.simpleUI.modifiers.InfoText;
import system.EventManager;
import tec.app.mobile.tecmap.POI.PointOfInterest;
import tec.app.mobile.tecmap.POIActivity;
import tec.app.mobile.tecmap.sensor.IGyroAccel;
import tec.app.mobile.tecmap.sensor.ISamplingService;
import tec.app.mobile.tecmap.sensor.SamplingService;
import util.IO;
import util.Log;
import util.Vec;
import worldData.RenderableEntity;
import worldData.SystemUpdater;
import worldData.Updateable;
import worldData.World;

/**
 * Created by fabianferihumer on 07.02.15.
 */
public class TECSetup extends TECMapAbstractSetup {

    private static final String LOG_TAG = "TECSetup";

    private Map<MeshComponent, PointOfInterest> buttonPoints;

    private GL1Renderer myRenderer;
    private boolean addObjCalledOneTime;
    private ActionWaitForAccuracy minAccuracyAction;
    private Action rotateGLCameraAction;
    private RadarView radar;
    private ActionCalcRelativePos gpsMovementAction;

    private float totalTimeDelta;

    public TECSetup(Context context, List<PointOfInterest> pois) {
        super(context, pois, null);
    }

    @Override
    public void _a_initFieldsIfNecessary() {
        buttonPoints = new HashMap<>();
        totalTimeDelta = 0;
        world = new World(camera) {
            @Override
            public void drawElements(GLCamera camera, GL10 gl) {
                if (container != null) {
                    for (int i = 0; i < container.myLength; i++) {
                        RenderableEntity renderableEntity = container.get(i);
                        if (renderableEntity != null) {
                            Vec pos = ((HasPosition) renderableEntity).getPosition().copy()
                                    .sub(myCamera.getPosition());

                            float length = pos.getLength();
                            if (length <= TECMapRadarView.RADAR_RADIUS) {
                                renderableEntity.render(gl, this);
                            }
                        }
                    }
                }
            }

            @Override
            public boolean update(float timeDelta, Updateable parent) {
                // SCALE Buttons according to distance, otherwise the ones far away are too small
                if (container != null) {
                    totalTimeDelta += timeDelta;
                    for (int i = 0; i < container.myLength; i++) {
                        if (container.get(i) != null) {
                            Vec pos = ((HasPosition) container.get(i)).getPosition().copy()
                                    .sub(myCamera.getPosition());
                            GeoObj obj = ((GeoObj) container.get(i));
                            MeshComponent comp = obj.getGraphicsComponent();
                            float length = pos.getLength();
                            float scaleFactor = (float) Math.sqrt(length);
                            if (length <= TECMapRadarView.RADAR_RADIUS && totalTimeDelta > 5) {
                                PointOfInterest poi = buttonPoints.get(comp);
                                setUI(obj, poi, i, GLFactory.getInstance());
                                //buttonPoints.remove(comp);
                                comp = obj.getGraphicsComponent();
                                comp.setScale(new Vec(1 + scaleFactor, 1 + scaleFactor, 1 + scaleFactor));
                                Log.d(LOG_TAG, "Updated GUI");
                            }
                            /*if (length <= 10)
                                comp.removeAllAnimations();*/
                            /*else if (!comp.hasAnimation()) {
                                comp.addChild(new AnimationFaceToCamera(camera, 0.01f));
                            }*/
                        }
                    }
                    if (totalTimeDelta > 5) {
                        //TextureManager.reloadTexturesIfNeeded();
                        totalTimeDelta = 0;
                    }
                }
                return super.update(timeDelta, parent);
            }
        };
        radar = new TECMapRadarView(getActivity(), camera, world.getAllItems());
    }

    @Override
    public void _b_addWorldsToRenderer(GL1Renderer glRenderer, GLFactory objectFactory, GeoObj currentPosition) {
        myRenderer = glRenderer;
        glRenderer.addRenderElement(world);
    }

    @Override
    public void _c_addActionsToEvents(final EventManager eventManager, CustomGLSurfaceView arView, SystemUpdater updater) {
        rotateGLCameraAction = new ActionRotateCameraBuffered(camera);
        eventManager.addOnOrientationChangedAction(rotateGLCameraAction);
        gpsMovementAction = new ActionCalcRelativePos(world, camera);
        eventManager.addOnLocationChangedAction(gpsMovementAction);
        minAccuracyAction = new ActionWaitForAccuracy(getActivity(), 100.0f, 100) {
            @Override
            public void minAccuracyReachedFirstTime(Location l,
                                                    ActionWaitForAccuracy a) {
                Log.e("ADD", "ADDING OBJECTS TO WORLD!!!!");
                callAddObjectsToWorldIfNotCalledAlready();
                if (!eventManager.getOnLocationChangedAction().remove(a)) {
                    /*Log.e(LOG_TAG,
                            "Could not remove minAccuracyAction from the onLocationChangedAction list");*/
                }
            }
        };
        eventManager.addOnLocationChangedAction(minAccuracyAction);
    }

    protected void callAddObjectsToWorldIfNotCalledAlready() {
        if (!addObjCalledOneTime) {
            addObjectsTo(myRenderer, world, GLFactory.getInstance());
            addObjCalledOneTime = true;
        } else {
            Log.w(LOG_TAG, "callAddObjectsToWorldIfNotCalledAlready() "
                    + "called more then one time!");
        }
    }

    private void addObjectsTo(GL1Renderer myRenderer, World world, GLFactory objectFactory) {
        int counter = 0;
        for (final PointOfInterest poi : pois) {
            // transform android ui elements into openGL models:
            GeoObj obj = new GeoObj(poi.lat, poi.lng, poi.height, null);
            setUI(obj, poi, counter++, objectFactory);
            world.add(obj);
        }
    }

    private void setUI(GeoObj obj, final PointOfInterest poi, int index, GLFactory objectFactory) {
        float distance = (float) camera.getGPSPositionAsGeoObj().getDistance(obj);

        ModifierGroup l = new ModifierGroup(Theme.A(getActivity(),
                Theme.ThemeColors.initToGray()));

        if (distance < 100) {
            l.addModifier(new Headline(poi.logo, poi.title, 26.0f));
            l.addModifier(new InfoText(poi.info, Gravity.LEFT, 22.0f));
        } else
            l.addModifier(new Headline(poi.title, distance / 4.0f));

        l.addModifier(new InfoText(Math.round(distance * 100) / 100.0f + " m", Gravity.LEFT, distance < 100 ? 25.0f : distance / 4.0f));
        View v = l.getView(getActivity());

        String bitmapName = poi.title + index;
        Bitmap bitmap = IO.loadBitmapFromView(v, View.MeasureSpec.makeMeasureSpec(400,
                View.MeasureSpec.AT_MOST), LinearLayout.LayoutParams.WRAP_CONTENT);

        if (obj.hasComponent(MeshComponent.class)) {
            TexturedShape s = new TexturedShape(bitmapName, bitmap);
        } else {
            MeshComponent button = objectFactory.newTexturedSquare(bitmapName, bitmap);
            button.setOnClickCommand(new Command() {
                @Override
                public boolean execute() {
                    Intent i = new Intent(context, POIActivity.class);
                    i.putExtra("POI", poi);
                    ((Activity) context).startActivityForResult(i, 0);
                    return true;
                }
            });

            button.addAnimation(new AnimationFaceToCamera(camera, 0.001f, button));
            button.setScale(new Vec(2f, 2f, 2f));

            buttonPoints.put(button, poi);

            obj.setComp(button);
        }
    }

    @Override
    public void _d_addElementsToUpdateThread(SystemUpdater updater) {
        updater.addObjectToUpdateCycle(world);
        updater.addObjectToUpdateCycle(radar);
        updater.addObjectToUpdateCycle(rotateGLCameraAction);
        initSensoring();
    }

    @Override
    public void _e2_addElementsToGuiSetup(TECMapGuiSetup guiSetup, Activity activity) {
        guiSetup.setRadarView(radar);
    }

    private void initSensoring() {
        bindSamplingService();
        startSamplingService();
    }

    @Override
    public void onDestroy(Activity a) {
        Log.e("ON_DESTROY", "DESTROYING SAMPLING SERVICE!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        stopSampling();
        //releaseSamplingService();
        super.onDestroy(a);
    }

    @Override
    public void onStop(Activity a) {
        stopSampling();
        super.onStop(a);
    }

    private void startSamplingService() {
        if (samplingServiceRunning)    // shouldn't happen
            stopSamplingService();
        Intent i = new Intent(context, SamplingService.class);
        context.startService(i);
        samplingServiceRunning = true;
        Log.e("TAG", "STARTED SAMPLING SERVICE");
    }

    private void stopSamplingService() {
        Log.d(LOG_TAG, "stopSamplingService");
        if (samplingServiceRunning) {
            stopSampling();
            samplingServiceRunning = false;
        }
    }

    private void bindSamplingService() {
        samplingServiceConnection = new SamplingServiceConnection();
        Intent i = new Intent(context, SamplingService.class);
        context.bindService(i, samplingServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void releaseSamplingService() {
        releaseCallbackOnService();
        context.unbindService(samplingServiceConnection);
        samplingServiceConnection = null;
    }


    private void setCallbackOnService() {
        if (samplingService == null)
            Log.e(LOG_TAG, "setCallbackOnService: Service not available");
        else {
            try {
                samplingService.setCallback(iSteps.asBinder());
            } catch (DeadObjectException ex) {
                Log.e(LOG_TAG, "DeadObjectException");
            } catch (RemoteException ex) {
                Log.e(LOG_TAG, "RemoteException");
            }
        }
    }

    private void releaseCallbackOnService() {
        if (samplingService == null)
            Log.e(LOG_TAG, "releaseCallbackOnService: Service not available");
        else {
            try {
                samplingService.removeCallback();
            } catch (DeadObjectException ex) {
                Log.e(LOG_TAG, "DeadObjectException");
            } catch (RemoteException ex) {
                Log.e(LOG_TAG, "RemoteException");
            }
        }
    }

    private void updateSamplingServiceRunning() {
        if (samplingService == null)
            Log.e(LOG_TAG, "updateSamplingServiceRunning: Service not available");
        else {
            try {
                samplingServiceRunning = samplingService.isSampling();
            } catch (DeadObjectException ex) {
                Log.e(LOG_TAG, "DeadObjectException");
            } catch (RemoteException ex) {
                Log.e(LOG_TAG, "RemoteException");
            }
        }
    }

    private void updateState() {
        if (samplingService == null)
            Log.e(LOG_TAG, "updateState: Service not available");
        else {
            try {
                state = samplingService.getState();
            } catch (DeadObjectException ex) {
                Log.e(LOG_TAG, "DeadObjectException");
            } catch (RemoteException ex) {
                Log.e(LOG_TAG, "RemoteException");
            }
        }
    }

    private void stopSampling() {
        Log.d(LOG_TAG, "stopSampling");
        if (samplingService == null)
            Log.e(LOG_TAG, "stopSampling: Service not available");
        else {
            try {
                samplingService.stopSampling();
            } catch (DeadObjectException ex) {
                Log.e(LOG_TAG, "DeadObjectException");
            } catch (RemoteException ex) {
                Log.e(LOG_TAG, "RemoteException");
            }
        }
    }

    private IGyroAccel.Stub iSteps
            = new IGyroAccel.Stub() {

        double[] totalChange = new double[3];

        @Override
        public void diff(double x, double y, double z) {
            /*totalChange[0] += z;
            totalChange[1] += y;
            totalChange[2] += x;
            ((TextView) TECSetup.this.getActivity().findViewById(R.id.totalX)).setText("Total X: " + totalChange[0] + "");
            ((TextView) TECSetup.this.getActivity().findViewById(R.id.totalY)).setText("Total Y: " + totalChange[1] + "");
            ((TextView) TECSetup.this.getActivity().findViewById(R.id.totalZ)).setText("Total Z: " + totalChange[2] + "");
            //camera.changeNewPosition((float) z, (float) y, (float) x);
            Log.e("CHANGE", x + " || " + y + " || " + z);
            if (totalDistance(totalChange) >= 1) {
                gpsMovementAction.shouldUpdateLocation = true;
                totalChange[0] = 0;
                totalChange[1] = 0;
                totalChange[2] = 0;
            }*/
        }

        private double totalDistance(double[] totalChange) {
            return Math.sqrt(Math.pow(totalChange[0], 2) + Math.pow(totalChange[1], 2) + Math.pow(totalChange[2], 2));
        }

    };

    private int state = SamplingService.ENGINESTATES_IDLE;
    private ISamplingService samplingService = null;
    private SamplingServiceConnection samplingServiceConnection = null;
    private boolean samplingServiceRunning = false;

    class SamplingServiceConnection implements ServiceConnection {

        public void onServiceConnected(ComponentName className,
                                       IBinder boundService) {
            Log.d(LOG_TAG, "onServiceConnected");
            samplingService = ISamplingService.Stub.asInterface(boundService);
            setCallbackOnService();
            updateSamplingServiceRunning();
            updateState();

            if (state == SamplingService.ENGINESTATES_MEASURING)
                Log.d(LOG_TAG, "Measuring");
            Log.d(LOG_TAG, "onServiceConnected");
        }

        public void onServiceDisconnected(ComponentName className) {
            samplingService = null;
            Log.d(LOG_TAG, "onServiceDisconnected");
        }

    }

}
