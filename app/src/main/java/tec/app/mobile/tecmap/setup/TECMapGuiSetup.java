package tec.app.mobile.tecmap.setup;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;

import gui.GuiSetup;
import tec.app.mobile.tecmap.R;
import tec.app.mobile.tecmap.search.SearchResultAdapter;

/**
 * Created by Fabian Ferihumer on 09.02.15.
 */
public class TECMapGuiSetup extends GuiSetup {

    private LinearLayout radarView;
    private SearchView sv;
    private ImageView blur;
    private ListView searchResults;
    private TableLayout filters;
    private TableRow firstRow;
    private SearchResultAdapter resultAdapter;

    /**
     * @param setup
     * @param source
     */
    public TECMapGuiSetup(TECMapAbstractSetup setup, View source) {
        super(setup, source);
        radarView = (LinearLayout) source.findViewById(R.id.radarLayout);

        sv = (SearchView) source.findViewById(R.id.searchContent);
        blur = (ImageView) source.findViewById(R.id.blurOnSearch);
        searchResults = (ListView) source.findViewById(R.id.searchResults);
        filters = (TableLayout) source.findViewById(R.id.filters);
        firstRow = (TableRow) source.findViewById(R.id.firstRow);

        int column = 1;
        for (final String filter : SearchResultAdapter.filter) {
            CheckBox checkBox = new CheckBox(source.getContext());
            checkBox.setText(filter);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        resultAdapter.addFilter(filter);
                    else
                        resultAdapter.removeFilter(filter);
                    resultAdapter.filter();
                }
            });
            firstRow.addView(checkBox, new TableRow.LayoutParams(column++));
        }

        resultAdapter = new SearchResultAdapter(setup.getActivity(), setup.pois, setup.camera);
        searchResults.setAdapter(resultAdapter);

        sv.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                radarView.setVisibility(View.VISIBLE);
                blur.setVisibility(View.INVISIBLE);
                searchResults.setVisibility(View.INVISIBLE);
                filters.setVisibility(View.INVISIBLE);
                return false;
            }
        });
        sv.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radarView.setVisibility(View.INVISIBLE);
                searchResults.setVisibility(View.VISIBLE);
                blur.setVisibility(View.VISIBLE);
                filters.setVisibility(View.VISIBLE);
                resultAdapter.changedData();
            }
        });
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                resultAdapter.filter(newText);
                return false;
            }
        });
    }

    public void setRadarView(View v) {
        if (v.getParent() != null)
            ((LinearLayout) v.getParent()).removeView(v);
        radarView.addView(v, 0);
    }
}
