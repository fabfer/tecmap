package tec.app.mobile.tecmap.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by fabianferihumer on 11.02.15.
 */
public class ImageUtils {

    public static Bitmap getBitmapFromAsset(Context context, String fileName) {
        AssetManager assetManager = context.getAssets();

        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = assetManager.open("img/" + fileName);
            bitmap = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            // handle exception
        } finally {
            if (is != null)
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return bitmap;
    }

}
