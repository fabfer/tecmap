package tec.app.mobile.tecmap.POI;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabianferihumer on 10.02.15.
 */
public class PointOfInterest implements Parcelable {

    public String title, info;
    public String imgURL;
    public double lat, lng, height;
    public List<String> filter;
    public Bitmap logo;
    public float distance;

    public PointOfInterest(Parcel in) {
        this.title = in.readString();
        this.info = in.readString();
        this.imgURL = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.height = in.readDouble();
        this.logo = in.readParcelable(Bitmap.class.getClassLoader());
        filter = new ArrayList<>();
        in.readStringList(this.filter);
    }

    public PointOfInterest(String title, String info, String imgURL, double lat, double lng, double height, List<String> filters) {
        this.title = title;
        this.info = info;
        this.imgURL = imgURL;
        this.lat = lat;
        this.lng = lng;
        this.height = height;
        this.filter = filters;
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<PointOfInterest> CREATOR = new Parcelable.Creator<PointOfInterest>() {
        public PointOfInterest createFromParcel(Parcel in) {
            return new PointOfInterest(in);
        }

        public PointOfInterest[] newArray(int size) {
            return new PointOfInterest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(info);
        dest.writeString(imgURL);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeDouble(height);
        dest.writeParcelable(logo, 0);
        dest.writeStringList(filter);
    }

}
