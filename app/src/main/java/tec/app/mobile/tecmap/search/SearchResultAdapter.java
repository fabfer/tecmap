package tec.app.mobile.tecmap.search;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import geo.GeoObj;
import gl.GLCamera;
import tec.app.mobile.tecmap.POI.PointOfInterest;
import tec.app.mobile.tecmap.POIActivity;
import tec.app.mobile.tecmap.R;

/**
 * Created by Fabian Ferihumer on 30.01.15.
 */
public class SearchResultAdapter extends BaseAdapter /*implements OnClickListener*/ {

	/*private class OnItemClickListener implements OnClickListener{
        private int mPosition;
	    OnItemClickListener(int position){
	            mPosition = position;
	    }
	    public void onClick(View arg0) {
	            Log.v("ddd", "onItemClick at position" + mPosition);
	    }
	}*/

    public static String[] filter = {"Sport", "Food", "Study", "Shop", "Cultural", "Financial", "Assistance"};

    private Activity activity;
    private List<PointOfInterest> resultList;
    private List<PointOfInterest> pointsOfInterest;

    private List<String> activeFilters;
    private String currentText;

    private GLCamera myCamera;

    public SearchResultAdapter(Activity activity, List<PointOfInterest> pointsOfInterests, GLCamera glCamera) {
        this.activity = activity;
        this.resultList = new ArrayList<>();
        resultList.addAll(pointsOfInterests);
        this.pointsOfInterest = pointsOfInterests;
        activeFilters = new ArrayList<>();
        this.myCamera = glCamera;
        currentText = "";
    }

    public int getCount() {
        return resultList.size();
    }

    public Object getItem(int position) {
        return resultList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public void addFilter(String filter) {
        activeFilters.add(filter);
    }

    public void removeFilter(String filter) {
        activeFilters.remove(filter);
    }

    public void removeAllFilters() {
        activeFilters.clear();
    }

    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = activity.getLayoutInflater().inflate(R.layout.row, null);
        }
        TextView title = (TextView) view.findViewById(R.id.poiTitle);
        ImageView imgView = (ImageView) view.findViewById(R.id.poiIcon);
        TextView info = (TextView) view.findViewById(R.id.poiInfo);

        final PointOfInterest poi = resultList.get(position);
        title.setText(poi.title);
        info.setText(Math.round(poi.distance * 100) / 100.0f + " m");
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, POIActivity.class);
                i.putExtra("POI", poi);
                activity.startActivityForResult(i, 0);
            }
        });

        imgView.setImageBitmap(poi.logo);

        return view;
    }

    // Filter Class
    public void filter(String searchText) {
        currentText = searchText;
        searchText = searchText.toLowerCase(Locale.getDefault());
        resultList.clear();
        for (PointOfInterest poi : pointsOfInterest) {
            if ((searchText.length() == 0 || poi.title.toLowerCase(Locale.getDefault())
                    .contains(searchText)) && (activeFilters.isEmpty() || containsAny(activeFilters, poi.filter))) {
                resultList.add(poi);
            }
        }
        changedData();
    }

    private boolean containsAny(List<String> filters1, List<String> filters2) {
        for (String f1 : filters2) {
            if (filters1.contains(f1))
                return true;
        }
        return false;
    }

    public void changedData() {
        for (PointOfInterest poi : pointsOfInterest) {
            GeoObj pos = new GeoObj(poi.lat, poi.lng, poi.height);
            float distance = pos.getPosition().sub(myCamera.getPosition()).getLength();
            poi.distance = distance;
        }
        Collections.sort(resultList, new Comparator<PointOfInterest>() {
            @Override
            public int compare(PointOfInterest lhs, PointOfInterest rhs) {
                return Float.compare(lhs.distance, rhs.distance);
            }
        });
        notifyDataSetChanged();
    }

    public void filter() {
        filter(currentText);
    }
}