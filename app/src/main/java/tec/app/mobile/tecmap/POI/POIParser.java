package tec.app.mobile.tecmap.POI;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabianferihumer on 10.02.15.
 */
public class POIParser {

    private static final String ns = null;

    public static List<PointOfInterest> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readPOIs(parser);
        } finally {
            in.close();
        }
    }

    private static List<PointOfInterest> readPOIs(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<PointOfInterest> pois = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "POIs");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the POI tag
            if (name.equals("POI")) {
                pois.add(readPOI(parser));
            } else {
                skip(parser);
            }
        }
        return pois;
    }

    private static PointOfInterest readPOI(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "POI");
        String poiName = null;
        String info = null;
        String iconName = null;
        double longitude = 0, latitude = 0;
        float height = 0;
        List<String> filters = new ArrayList<>();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                parser.require(XmlPullParser.START_TAG, ns, "title");
                poiName = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "title");
            } else if (name.equals("info")) {
                parser.require(XmlPullParser.START_TAG, ns, "info");
                info = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "info");
            } else if (name.equals("icon-name")) {
                parser.require(XmlPullParser.START_TAG, ns, "icon-name");
                iconName = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "icon-name");
            } else if (name.equals("long")) {
                parser.require(XmlPullParser.START_TAG, ns, "long");
                longitude = readDouble(parser);
                parser.require(XmlPullParser.END_TAG, ns, "long");
            } else if (name.equals("lat")) {
                parser.require(XmlPullParser.START_TAG, ns, "lat");
                latitude = readDouble(parser);
                parser.require(XmlPullParser.END_TAG, ns, "lat");
            } else if (name.equals("height")) {
                parser.require(XmlPullParser.START_TAG, ns, "height");
                height = readFloat(parser);
                parser.require(XmlPullParser.END_TAG, ns, "height");
            } else if (name.equals("filters")) {
                parser.require(XmlPullParser.START_TAG, ns, "filters");
                filters = readFilters(parser);
                parser.require(XmlPullParser.END_TAG, ns, "filters");
            } else {
                skip(parser);
            }
        }
        return new PointOfInterest(poiName, info, iconName, latitude, longitude, height, filters);
    }

    private static List<String> readFilters(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<String> result = new ArrayList<>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            parser.require(XmlPullParser.START_TAG, ns, "filter");
            result.add(readText(parser));
            parser.require(XmlPullParser.END_TAG, ns, "filter");
        }
        return result;
    }

    private static float readFloat(XmlPullParser parser) throws IOException, XmlPullParserException {
        return (float) readDouble(parser);
    }

    private static double readDouble(XmlPullParser parser) throws IOException, XmlPullParserException {
        double result = 0;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Double.parseDouble(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private static void skip(XmlPullParser parser) throws IOException, XmlPullParserException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
