package tec.app.mobile.tecmap.search;

/**
 * Created by Fabian Ferihumer on 30.01.15.
 */
public class SearchResult {

    private String header;
    private String subtitle;
    private int imageId;

    public SearchResult(String header, String subtitle, int imageId) {
        this.header = header;
        this.subtitle = subtitle;
        this.imageId = imageId;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
