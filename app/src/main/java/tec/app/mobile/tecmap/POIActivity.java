package tec.app.mobile.tecmap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import tec.app.mobile.tecmap.POI.PointOfInterest;


public class POIActivity extends ActionBarActivity {

    private PointOfInterest poi;
    public GoogleMap gm;
    public String uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poi_layout);

        poi = getIntent().getParcelableExtra("POI");
        ((TextView)findViewById(R.id.title)).setText(poi.title);
        ((TextView)findViewById(R.id.info)).setText(poi.info);
        ((TextView)findViewById(R.id.height)).setText("Height: " + poi.height + "");
        ((TextView)findViewById(R.id.tags)).setText("Tags: " + poi.filter);
        ((ImageView) findViewById(R.id.logo)).setImageBitmap(poi.logo);

        MapFragment mf = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        gm = mf.getMap();

        gm.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(poi.lat, poi.lng), 16));
        gm.setMyLocationEnabled(true);

        gm.addMarker(new MarkerOptions().position(new LatLng(poi.lat, poi.lng)).title(poi.title));

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_poi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showSharingOptions(View v){
        String location = poi.lat + "," + poi.lng;
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = poi.title + ": http://maps.google.com/maps?q=loc:" + location;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "I'm at TEC! Check out my location!");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        //sharingIntent.putExtra(android.content.Intent.ACTION_SEND, Toast.makeText(this.getBaseContext(),"Message sent!"));
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

}
