package tec.app.mobile.tecmap.setup;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import gl.GLCamera;
import gui.RadarView;
import tec.app.mobile.tecmap.R;
import util.EfficientList;
import worldData.RenderableEntity;

/**
 * Created by Fabian Ferihumer on 09.02.15.
 */
public class TECMapRadarView extends RadarView {

    public static final int RADAR_RADIUS = 200;

    private Bitmap radar;
    private Bitmap customBackground;

    public TECMapRadarView(Activity myTargetActivity, GLCamera camera, EfficientList<RenderableEntity> items) {
        super(myTargetActivity, RADAR_RADIUS, camera, items);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(params);
        radar = BitmapFactory.decodeResource(getResources(), R.drawable.radar);
        customBackground = Bitmap.createBitmap(mySize, mySize, Bitmap.Config.ARGB_8888);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBackGround(canvas);

        if (items != null)
            drawItems(canvas);
    }

    @Override
    protected void drawBackGround(Canvas canvas) {
        canvas.drawBitmap(getCustomBackground(mySize), 0, 0, paint);
    }

    @Override
    protected void drawItems(Canvas canvas) {
        super.drawItems(canvas);
    }

    private Bitmap getCustomBackground(int size) {
        Canvas c = new Canvas(customBackground);

        Paint p = new Paint();
        p.setColor(Color.CYAN);

        c.drawBitmap(radar, null, new Rect(0, 0, size, size), p);

        return customBackground;
    }

    @Override
    public void onResizeEvent(int recommendedHeight, int recommendedWidth) {
        super.onResizeEvent(recommendedHeight, recommendedWidth);
        customBackground = Bitmap.createBitmap(mySize, mySize, Bitmap.Config.ARGB_8888);
    }
}
