package tec.app.mobile.tecmap.POI;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import tec.app.mobile.tecmap.ExploreActivity;

/**
 * Created by fabianferihumer on 13.03.15.
 */
public class POILoader extends AsyncTask<String, Void, InputStream> {

    private ExploreActivity activity;

    public POILoader(ExploreActivity activity) {
        this.activity = activity;
    }

    @Override
    protected InputStream doInBackground(String... params) {
        HttpGet get = new HttpGet(params[0]);
        StringBuilder sb = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        JSONObject jo = null;

        try {
            HttpResponse resp = client.execute(get);
            StatusLine sl = resp.getStatusLine();
            int code = sl.getStatusCode();

            if (code == 200) {
                HttpEntity entity = resp.getEntity();
                InputStream is = entity.getContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                String currentLine;
                while ((currentLine = br.readLine()) != null) {
                    sb.append(currentLine);
                }
                return new ByteArrayInputStream(sb.toString().getBytes());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(InputStream is) {
        if (is != null) {
            activity.parsePOIs(is);
        } else {
            activity.failedToLoad("Error while loading: Please check your internet connection & restart the application!");
        }
    }
}
