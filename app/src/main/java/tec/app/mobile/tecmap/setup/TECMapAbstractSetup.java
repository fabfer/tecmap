package tec.app.mobile.tecmap.setup;

import android.app.Activity;
import android.content.Context;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import java.util.List;

import gl.GLCamera;
import gui.GuiSetup;
import system.Setup;
import tec.app.mobile.tecmap.POI.PointOfInterest;
import tec.app.mobile.tecmap.R;
import util.Vec;
import worldData.World;

/**
 * Created by Fabian Ferihumer on 09.02.15.
 */
public abstract class TECMapAbstractSetup extends Setup {

    static {
        defaultArLayoutId = R.layout.activity_explore;
    }

    protected List<PointOfInterest> pois;
    protected Context context;
    protected GLCamera camera;
    protected World world;
    private View sourceView;

    protected TECMapAbstractSetup(Context ctx, List<PointOfInterest> pois, World world) {
        super();
        this.context = ctx;
        this.pois = pois;
        this.camera = new GLCamera(new Vec(0, 0, getMeanHeight(pois)));
        this.world = world;
    }

    private float getMeanHeight(List<PointOfInterest> pois) {
        float height = 0;
        for (PointOfInterest poi : pois) {
            height += poi.height;
        }
        return height / (float) pois.size();
    }

    @Override
    public void _e1_addElementsToOverlay(FrameLayout overlayView, Activity activity) {
        // the main.xml layout is loaded and the guiSetup is created for
        // customization. then the customized view is added to overlayView
        sourceView = View.inflate(activity, defaultArLayoutId, null);
        guiSetup = new TECMapGuiSetup(this, sourceView);

        _e2_addElementsToGuiSetup(getGuiSetup(), activity);
        overlayView.addView(sourceView);
    }

    @Override
    public void _e2_addElementsToGuiSetup(GuiSetup guiSetup, Activity activity) {
        _e2_addElementsToGuiSetup((TECMapGuiSetup) guiSetup, activity);
    }

    public abstract void _e2_addElementsToGuiSetup(TECMapGuiSetup guiSetup, Activity activity);

    @Override
    public void changeOrientation(int orientation) {
        myOverlayView.removeView(sourceView);
        sourceView = null;
        _e1_addElementsToOverlay(myOverlayView, getActivity());

        Display display = ((WindowManager) getActivity().getSystemService(
                Activity.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = 0;
        try {
            rotation = (Integer) display.getClass()
                    .getMethod("getRotation", null).invoke(display, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        screenOrientation = rotation;
        /*switch (screenOrientation) {
            case Surface.ROTATION_0:
                camera.setRotation(0, 0, 0);
                world.setMyRotation(new Vec(0, 0, 0));
                break;
            case Surface.ROTATION_90:
                camera.setRotation(-90, 0, 0);
                world.setMyRotation(new Vec(90, 0, 0));
                break;
            case Surface.ROTATION_180:
                camera.setRotation(180, 0, 0);
                world.setMyRotation(new Vec(180, 0, 0));
                break;
            case Surface.ROTATION_270:
                camera.setRotation(90, 0, 0);
                world.setMyRotation(new Vec(270, 0, 0));
                break;
        }*/
        /*if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            camera.setRotation(0, 0, 0);
        } else {
            camera.setRotation(-90, 0, 0);
        }*/
    }
}
