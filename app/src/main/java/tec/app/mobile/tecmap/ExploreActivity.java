package tec.app.mobile.tecmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import system.ArActivity;
import tec.app.mobile.tecmap.POI.POILoader;
import tec.app.mobile.tecmap.POI.POIParser;
import tec.app.mobile.tecmap.POI.PointOfInterest;
import tec.app.mobile.tecmap.setup.TECSetup;


public class ExploreActivity extends ActionBarActivity {

    private List<PointOfInterest> pois;
    private int loadedPois;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_screen);
        retry();
    }

    public void parsePOIs(InputStream is) {
        try {
            pois = POIParser.parse(is);
            for (PointOfInterest poi : pois) {
                new ImgLoader(poi).execute();
            }
        } catch (XmlPullParserException e) {
            failedToLoad("Error while loading: Please restart your application!");
            e.printStackTrace();
        } catch (IOException e) {
            failedToLoad("Error while loading: Please check your internet connection & restart the application!");
            e.printStackTrace();
        }
    }

    private synchronized void increaseLoadedPOIs() {
        loadedPois++;
        if (loadedPois == pois.size()) {
            startActivity();
        }
    }

    public void retry(View v) {
        retry();
    }

    public void retry() {
        failedToLoad("Loading...");
        findViewById(R.id.retry).setVisibility(View.INVISIBLE);
        loadedPois = 0;
        //new POILoader(this).execute("http://eduardoregalado.me/safe/public.php?service=files&t=2800264fb31bdfb1c3e4228c79a7b381&download");
        new POILoader(this).execute("http://pastebin.com/raw.php?i=gB0bRfeC");
    }

    public void startActivity() {
        ArActivity.startWithSetup(this, new TECSetup(this, pois));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_explore, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void failedToLoad(String message) {
        ((TextView) findViewById(R.id.info)).setText(message);
        findViewById(R.id.retry).setVisibility(View.VISIBLE);
    }

    class ImgLoader extends AsyncTask<String, Void, Bitmap> {

        private PointOfInterest poi;

        public ImgLoader(PointOfInterest poi) {
            this.poi = poi;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            URL url = null;
            try {
                url = new URL(poi.imgURL);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bmp;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            poi.logo = bitmap;
            ExploreActivity.this.increaseLoadedPOIs();
        }
    }

}