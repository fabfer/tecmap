package simpleUi.uiDecoration;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import simpleUi.uiDecoration.*;
import simpleUi.util.BGUtils;
import simpleUi.util.TextUtils;

/**
 * This class can be used if not every decorate method should be implemented and
 * only specific elements should be decorated
 * <p/>
 * use {@link simpleUi.util.BGUtils} and {@link simpleUi.util.TextUtils} to decorade the views in the
 * {@link simpleUi.uiDecoration.DefaultUiDecorator#decorate(android.content.Context, android.view.View, int, int)} methods.
 *
 * @author Simon Heinen
 */
public class DefaultUiDecorator implements simpleUi.uiDecoration.UiDecorator {
    private static final String LOG_TAG = "DefaultUiDecorator";
    private int myCurrentLevel;
    private boolean showInfoIfDecorationNotSpecified;

    /**
     * read {@link simpleUi.uiDecoration.DefaultUiDecorator}
     *
     * @param showDebugInfoIfDecorationNotSpecified
     */
    public DefaultUiDecorator(boolean showDebugInfoIfDecorationNotSpecified) {
        this.showInfoIfDecorationNotSpecified = showDebugInfoIfDecorationNotSpecified;
    }

    @Override
    public int getCurrentLevel() {
        return myCurrentLevel;
    }

    @Override
    public void setCurrentLevel(int currentLevel) {
        myCurrentLevel = currentLevel;
    }

    private void showInfo(View targetView, int level, int type) {
        if (showInfoIfDecorationNotSpecified) {
            Log.i(LOG_TAG, "No decoration for "
                    + targetView.getClass().toString() + " (level=" + level
                    + ", UiDecorator.type=" + type);
        }
    }

    @Override
    public boolean decorate(Context context, View targetView, int level,
                            int type) {
        showInfo(targetView, level, type);
        return false;
    }

    @Override
    public boolean decorate(Context context, ImageView targetView, int level,
                            int type) {
        showInfo(targetView, level, type);
        return false;
    }

    @Override
    public boolean decorate(Context context, Button targetView, int level,
                            int type) {
        showInfo(targetView, level, type);
        return false;
    }

    @Override
    public boolean decorate(Context context, TextView targetView, int level,
                            int type) {
        showInfo(targetView, level, type);
        return false;
    }

    @Override
    public boolean decorate(Context context, EditText targetView, int level,
                            int type) {
        showInfo(targetView, level, type);
        return false;
    }

}
