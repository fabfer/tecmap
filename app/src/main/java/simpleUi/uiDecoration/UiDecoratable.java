package simpleUi.uiDecoration;

import simpleUi.uiDecoration.*;

public interface UiDecoratable {

    /**
     * @param decorator e.g. the {@link simpleUi.uiDecoration.ExampleDecorator}
     * @return true if the decorator could be assigned to all children and their
     * sub-children
     */
    public boolean assignNewDecorator(simpleUi.uiDecoration.UiDecorator decorator);

}
