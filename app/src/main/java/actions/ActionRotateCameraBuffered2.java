package actions;

import actions.algos.BufferAlgo2;
import actions.algos.SensorAlgo1;
import gl.GLCamRotationController;

public class ActionRotateCameraBuffered2 extends ActionWithSensorProcessing {

    public ActionRotateCameraBuffered2(GLCamRotationController targetCamera) {
        super(targetCamera);
    }

    @Override
    public void initAlgos() {
        accelAlgo = new SensorAlgo1(0.2f);
        magnetAlgo = new SensorAlgo1(1.0f);
        orientAlgo = new SensorAlgo1(0.004f);// TODO find correct values

        accelBufferAlgo = new BufferAlgo2(0.2f);
        magnetBufferAlgo = new BufferAlgo2(0.2f);
    }

}
