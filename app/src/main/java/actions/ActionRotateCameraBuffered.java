package actions;

import actions.algos.SensorAlgo1;
import gl.GLCamRotationController;

public class ActionRotateCameraBuffered extends ActionWithSensorProcessing {

    public ActionRotateCameraBuffered(GLCamRotationController targetCamera) {
        super(targetCamera);
    }

    @Override
    public void initAlgos() {
        accelAlgo = new SensorAlgo1(0.1f);
        //magnetAlgo = new SensorAlgo1(1.0f);
        orientAlgo = new SensorAlgo1(0.001f);

        accelBufferAlgo = new BufferAlgo4();
        //magnetBufferAlgo = new BufferAlgo4();
    }

    @Override
    public synchronized boolean onAccelChanged(float[] values) {
        return false;
    }

    @Override
    public synchronized boolean onMagnetChanged(float[] values) {
        return false;
    }
}
