package actions;

/**
 * Created by fabianferihumer on 20.03.15.
 */
public class BufferAlgo4 extends actions.algos.Algo {

    private void rootMeanSquareBuffer(float[] target, float[] values) {
        final float amplification = 200.0f;
        float buffer = 20.0f;

        target[0] += amplification;
        target[1] += amplification;
        target[2] += amplification;
        values[0] += amplification;
        values[1] += amplification;
        values[2] += amplification;

        target[0] = (float) (Math.sqrt((target[0] * target[0] * buffer + values[0] * values[0]) / (1 + buffer)));
        target[1] = (float) (Math.sqrt((target[1] * target[1] * buffer + values[1] * values[1]) / (1 + buffer)));
        target[2] = (float) (Math.sqrt((target[2] * target[2] * buffer + values[2] * values[2]) / (1 + buffer)));

        target[0] -= amplification;
        target[1] -= amplification;
        target[2] -= amplification;
        values[0] -= amplification;
        values[1] -= amplification;
        values[2] -= amplification;
    }

    @Override
    public boolean execute(float[] targetValues, float[] newValues, float bufferSize) {
        rootMeanSquareBuffer(targetValues, newValues);
        return true;
    }
}
